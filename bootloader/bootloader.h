/**
  ******************************************************************************
  * STM32L4 Bootloader
  ******************************************************************************
  * @author Alan Rodriguez
  * @file   bootloader.h
  * @brief  Bootloader implementation
  *	        This file contains the functions of the bootloader. The bootloader
  *	        implementation uses the official HAL library of ST.
  * @see    Please refer to README for detailed information.
  ******************************************************************************
**/

#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#include "stdint.h"
#include "usart.h"
#include "string.h"
#include "aes.h"


#define APP_ADDRESS		(uint32_t)0X0800A800
#define END_ADDRESS		(uint32_t)0X08038800
#define CRC_ADDRESS		(uint32_t)0X0802D001
#define EEPROM_ADDRESS (uint32_t)0X08038800
#define APP_PAGES			98

#define FLASH_OFFSET	8

#define _1_BYTE_SHIFT 8
#define _2_BYTE_SHIFT 16
#define _3_BYTE_SHIFT 24
#define _4_BYTE_SHIFT 32
#define _5_BYTE_SHIFT 40
#define _6_BYTE_SHIFT 48
#define _7_BYTE_SHIFT 56

#define	LENGHT_BYTE 		1
#define	CRC_BYTES 			4

#define RESET_HANDLER_OFFSET 4


#define BL_UART huart2

#define BOOTLOADER_BUFFER_LENGTH 200

/* ACK and NACK bytes*/
#define BL_ACK   0XA5
#define BL_NACK  0X7F

/*
 * Definicion de los comando que soporta el bootloader
 */
#define		BL_GET_VER								0x51
#define 		BL_GET_HELP							0x52
#define 		BL_GET_CID								0x53
#define 		BL_GET_RDP_STATUS				0x54
#define 		BL_GO_TO_ADDR						0x55
#define 		BL_FLASH_ERASE						0x56
#define 		BL_MEM_WRITE							0x57
#define BL_VERSION 1

/* Codigos de error para bootloader */
typedef enum
{
    BL_OK = 0,
    BL_NO_APP,
    BL_SIZE_ERROR,
    BL_CRC_ERROR,
    BL_ERASE_ERROR,
    BL_WRITE_ERROR,
    BL_OBP_ERROR,
	BL_FLASH_ERROR,
	BL_CMD_ERROR
}BL_StatusTypeDef;

enum
{
	ADDR_VALID = 0,
	ADDR_INVALID
};

typedef void (*pFunction)(void);

uint8_t bootloader_buffer[BOOTLOADER_BUFFER_LENGTH];


/*
 * Declaracion de las funciones necesarias
 */

BL_StatusTypeDef Bootloader_FlashBegin(void);
BL_StatusTypeDef Bootloader_FlashProgramm(uint64_t data, uint32_t address);
BL_StatusTypeDef Bootloader_FlashErasePg(uint32_t PgNumber, uint32_t address);
BL_StatusTypeDef Bootloader_ProgrammApp(uint8_t buffer[], uint32_t address);
BL_StatusTypeDef Bootloader_ReciveCmd(uint8_t* data_buffer);
BL_StatusTypeDef Bootloader_Getcallback(uint8_t* cmd_buffer);
BL_StatusTypeDef Bootloader_check_crc(uint8_t *pData, uint32_t len, uint32_t crc_host);
uint8_t Bootloader_get_version(void);
uint8_t Bootloader_get_rdp(void);
uint8_t Bootloader_verify_addr(uint32_t go_address);
uint16_t Bootloader_get_DGBMCU(void);
BL_StatusTypeDef Bootloader_get_crc(uint8_t *buff, uint32_t len,uint32_t crc_host);
void Bootloader_JumpToApplication(void);
void Bootloader_Init(void);
void Bootloader_send_NACK(void);
void Bootloader_send_ACK(uint8_t command_code, uint8_t follow_len);
void Bootloader_uart_write_data(uint8_t *pBuffer,uint32_t len);



#endif /* BOOTLOADER_H_ */
