/**
  ******************************************************************************
  * STM32L4 Bootloader
  ******************************************************************************
  * @author Alan Rodriguez
  * @file   bootloader.c
  * @brief  Bootloader implementation
  *	        This file contains the functions of the bootloader. The bootloader
  *	        implementation uses the official HAL library of ST.
  * @see    Please refer to README for detailed information.
  ******************************************************************************
**/

#include "bootloader.h"
#include "stm32l4xx_hal.h"
#include "bootloader_fn_cmd.h"
#include "DBG.h"
#include "crc.h"



typedef struct
{
	uint8_t cmd;
	const char* name;
	void (*func)(uint8_t*);
}cmdCallback_t;


static cmdCallback_t callbackArray[] =
{
		{0x51,"BL_GET_VER", &BL_GET_VER_callback},
		{0x52,"BL_GET_HELP", &BL_GET_HELP_callback},
		{0x53,"BL_GET_CID", &BL_GET_CID_callback},
		{0x54,"BL_GET_RDP", &BL_GET_RDP_STATUS_callback},
		{0x55,"BL_GO_TO_ADDR",&BL_GO_TO_ADDRESS_callback},
		{0x56,"BL_FLASH_ERASE",&BL_FLASH_ERASE_callback},
		{0x57,"BL_MEM_WRITE",&BL_MEM_WRITE_callback}
};


/*
 * Funcion para desbloquear la memoria flash del MCU
 * es necesario ddesbloquearla para poder hacer procesos
 * de borrado y escritura
 */
BL_StatusTypeDef Bootloader_FlashBegin(void)
{
	BL_StatusTypeDef status = BL_OK;

	/*
	 * Funcion para desbloqueo de la memoria flash
	 */
	if (HAL_FLASH_Unlock() != HAL_OK)
	{
		status = BL_FLASH_ERROR;
		return status;
	}

	return status;

}

/*
 * Funcion para
 */
BL_StatusTypeDef Bootloader_FlashProgramm(uint64_t data, uint32_t address)
{
	/*
	 * Se revisa que la direccion a escribir este dentro del espacio disponible
	 * de memoria
	 */
	if (!(address <= (FLASH_BASE + FLASH_SIZE - FLASH_OFFSET)) || (address < APP_ADDRESS))
	{
		HAL_FLASH_Lock();
		return BL_WRITE_ERROR;
	}

	/*
	 * Se programa la direccion en memoria con los datos necesarios
	 */
	if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,address,data) == HAL_OK)
	{
		/*
		 * Se compara los datos en memoria para saber si el proceso
		 * de escritura fue correcto
		 */
		if (*(uint64_t*)address != data)
		{
			HAL_FLASH_Lock();
			return BL_WRITE_ERROR;
		}
	}
	else
	{
		/*
		 * Si un error ocurre se bloquea la flash y se retorna un error
		 */
		HAL_FLASH_Lock();
		return BL_WRITE_ERROR;
	}

	return BL_OK;
}


BL_StatusTypeDef Bootloader_FlashErasePg(uint32_t PgNumber, uint32_t address)
{

	/*
	 * Estructura para el proceso de borrado
	 */
	FLASH_EraseInitTypeDef EraseInit = {0};

	/*
	 * Variable para almacenar los errores
	 */
	uint32_t error_code = 0x0;

	/*
	 * Inicializacion de la estructura
	 */
	EraseInit.Banks = FLASH_BANK_2;
	EraseInit.NbPages = address;
	EraseInit.Page = PgNumber;
	EraseInit.TypeErase = FLASH_TYPEERASE_PAGES;

	if(HAL_FLASHEx_Erase(&EraseInit,&error_code) != HAL_OK)
	{
		HAL_FLASH_Lock();
		return BL_ERASE_ERROR;
	}

	return BL_OK;
}


BL_StatusTypeDef Bootloader_ProgrammApp(uint8_t buffer[], uint32_t address)
{
	uint64_t data;

	/*
	 * Se graban los datos en la memoria flash
	 */
	if((address >= APP_ADDRESS) || (address < END_ADDRESS) )
	{
						data = ((unsigned long long)buffer[7] << _7_BYTE_SHIFT)|
							   ((unsigned long long)buffer[6] << _6_BYTE_SHIFT)|
							   ((unsigned long long)buffer[5] << _5_BYTE_SHIFT)|
							   ((unsigned long long)buffer[4] << _4_BYTE_SHIFT)|
							   ((unsigned long)buffer[3] << _3_BYTE_SHIFT) |
							   ((unsigned long)buffer[2] << _2_BYTE_SHIFT) |
							   ((unsigned long)buffer[1] << _1_BYTE_SHIFT) |
							   ((unsigned long)buffer[0]);

				if(Bootloader_FlashProgramm(data,address) != BL_OK)
				{
					/*
					 * Si una escritura no se lleva de manera correcta se
					 * detiene la escritura
					 * y se regresa un error
					 */
					HAL_FLASH_Lock();
					return BL_FLASH_ERROR;
				}
	}

	else
	{
		HAL_FLASH_Lock();
		return BL_FLASH_ERROR;
	}

	return BL_OK;
}

void Bootloader_JumpToApplication(void)
{
	   //just a function pointer to hold the address of the reset handler of the user app.
	    void (*app_reset_handler)(void);

	    HAL_RCC_DeInit();
	    HAL_DeInit();

	    SysTick->CTRL = 0;
	    SysTick->LOAD = 0;
	    SysTick->VAL  = 0;


	    // 1. configure the MSP by reading the value from the base address of the sector 2
	    uint32_t msp_value = *(volatile uint32_t *)APP_ADDRESS;

	    //This function comes from CMSIS.
	    __set_MSP(msp_value);

	    SCB->VTOR = 0x08000000 | 0xA800 ;

	    /* 2. Now fetch the reset handler address of the user application
	     * from the location FLASH_SECTOR2_BASE_ADDRESS+4
	     */
	    uint32_t resethandler_address = *(volatile uint32_t *) (APP_ADDRESS + 4);

	    app_reset_handler = (void*) resethandler_address;

	    //3. jump to reset handler of the user application
	    app_reset_handler();
}


void Bootloader_Init(void)
{
	Bootloader_FlashBegin();
#if 0
	  /*
	   * Funcion para iniciar la memoria flash antes de usarala
	   */
	  Bootloader_FlashBegin();

	  /*
       * Se borran todas las paginas disponibles para la aplicacion
       * apatir de la direccion de la aplicacion y hasta el area
       * reservada para la memoria eeprom
       */
	  Bootloader_FlashErasePg(APP_PAGES, (APP_ADDRESS - FLASH_BASE)/FLASH_PAGE_SIZE);
	  /*
	   * Grabar en la memoria flash el programa de la aplicacion
	   * desde la direccion asiganda
	   */
	  Bootloader_ProgrammApp(programm_buffer,APP_ADDRESS);
	  HAL_FLASH_Lock();
#endif
}


BL_StatusTypeDef Bootloader_ReciveCmd(uint8_t* data_buffer)
{
	uint8_t cmd_length = 0;

	/*
	 * El MCU entra en un loop para recivir todos los comandos provenientes
	 * del HOST hasta que reciva un comando de reinicio
	 */
	while(1)
	{
		memset(data_buffer,'\0',200);

		/*
		 * Primero se recibe el primer dato que corresponde a la longitud
		 * total del comando
		 */
		HAL_UART_Receive(&BL_UART,data_buffer,LENGHT_BYTE,HAL_MAX_DELAY);

		cmd_length = data_buffer[0];

		/*
		 * Se recibe una cantidad de datos igual a la longitud obtenida
		 * del host
		 */
		HAL_UART_Receive(&BL_UART,data_buffer + LENGHT_BYTE,cmd_length,HAL_MAX_DELAY);

		/*
		 * Se llama el callback necesario para atender el comando que llego
		 */
		if(Bootloader_Getcallback(data_buffer) == BL_CMD_ERROR)
		{
			DBG_println("Comando no valido");
			return BL_CMD_ERROR;
		}
	}

	return BL_OK;
}

/**********************************************************************/
BL_StatusTypeDef Bootloader_Getcallback(uint8_t* cmd_buffer)
{
	uint8_t cmd = *(cmd_buffer + LENGHT_BYTE);

	for(int i = 0; i < sizeof(callbackArray)/sizeof(callbackArray[0]);i++)
	{
		if(cmd == callbackArray[i].cmd)
		{
				callbackArray[i].func(cmd_buffer);
				return BL_OK;
		}
	}
	return BL_CMD_ERROR;
}

/***********************************************************************/
BL_StatusTypeDef Bootloader_check_crc(uint8_t *pData, uint32_t len, uint32_t crc_host)
{
    uint32_t uwCRCValue=0xFFFFFFFF;
    __HAL_CRC_DR_RESET(&hcrc);
    uwCRCValue = HAL_CRC_Calculate(&hcrc,(uint32_t*)pData,2);
#if 0
    for (uint32_t i=0 ; i < len ; i++)
	{
        uint32_t i_data = pData[i];
        uwCRCValue = HAL_CRC_Accumulate(&hcrc, &i_data, 1);
	}
#endif
	 /* Reset CRC Calculation Unit */
  __HAL_CRC_DR_RESET(&hcrc);

	if( uwCRCValue == crc_host)
	{
		return BL_OK;
	}

	return BL_CRC_ERROR;

}

void Bootloader_send_ACK(uint8_t command_code, uint8_t follow_len)
{

	//here we send 2 byte.. first byte is ack and the second byte is len value
	uint8_t ack_buf[2];
	ack_buf[0] = BL_ACK;
	ack_buf[1] = follow_len;
	HAL_UART_Transmit(&BL_UART,ack_buf,2,HAL_MAX_DELAY);

}

void Bootloader_send_NACK(void)
{
    uint8_t nack = BL_NACK;
    HAL_UART_Transmit(&BL_UART,&nack,1,HAL_MAX_DELAY);
}


void Bootloader_uart_write_data(uint8_t *pBuffer,uint32_t len)
{
    /*you can replace the below ST's USART driver API call with your MCUs driver API call */
        HAL_UART_Transmit(&BL_UART,pBuffer,len,HAL_MAX_DELAY);

}


//Just returns the macro value .
uint8_t Bootloader_get_version(void)
{
  return (uint8_t)BL_VERSION;
}

uint16_t Bootloader_get_DGBMCU(void)
{
	/*
		The STM32F446xx MCUs integrate an MCU ID code. This ID identifies the ST MCU partnumber
		and the die revision. It is part of the DBG_MCU component and is mapped on the
		external PPB bus (see Section 33.16 on page 1304). This code is accessible using the
		JTAG debug pCat.2ort (4 to 5 pins) or the SW debug port (two pins) or by the user software.
		It is even accessible while the MCU is under system reset. */
		uint16_t cid;
		cid = (uint16_t)(DBGMCU->IDCODE) & 0x0FFF;
		return  cid;
}


uint8_t Bootloader_get_rdp(void)
{
	uint8_t rdp_status=0;
	#if 1
	FLASH_OBProgramInitTypeDef  ob_handle;
	HAL_FLASHEx_OBGetConfig(&ob_handle);
	rdp_status = (uint8_t)ob_handle.RDPLevel;
	#else

	 volatile uint32_t *pOB_addr = (uint32_t*) 0x1FFFC000;
	 rdp_status =  (uint8_t)(*pOB_addr >> 8) ;
	#endif

	return rdp_status;

}

uint8_t Bootloader_verify_addr(uint32_t go_address)
{
	//so, what are the valid addresses to which we can jump ?
	//can we jump to system memory ? yes
	//can we jump to sram1 memory ?  yes
	//can we jump to sram2 memory ? yes
	//can we jump to backup sram memory ? yes
	//can we jump to peripheral memory ? its possible , but dont allow. so no
	//can we jump to external memory ? yes.

	//incomplete -poorly written .. optimize it

	if ( go_address >= APP_ADDRESS && go_address <= END_ADDRESS)
	{
		return ADDR_VALID;
	}
	else
		return ADDR_INVALID;
}


BL_StatusTypeDef Bootloader_get_crc(uint8_t *buff, uint32_t len,uint32_t crc_host)
{
    uint32_t i;

    uint32_t Crc = 0XFFFFFFFF;
	uint32_t n;
    for(n = 0 ; n < len ; n++ )
    {
        uint32_t data = buff[n];
        Crc = Crc ^ data;
        for(i=0; i<32; i++)
        {

        if (Crc & 0x80000000)
            Crc = (Crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
        else
            Crc = (Crc << 1);
        }

    }

	if( Crc == crc_host)
	{
		return BL_OK;
	}

	return BL_CRC_ERROR;
}

