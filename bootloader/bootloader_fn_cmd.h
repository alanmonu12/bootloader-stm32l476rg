/*
 * bootloader_fn_cmd.h
 *
 *  Created on: 21/12/2018
 *      Author: equipo
 */

/*Todas las variables necesarias para el manejo del cifrado */
/*KEY para desencriptar */


#ifndef BOOTLOADER_FN_CMD_H_
#define BOOTLOADER_FN_CMD_H_

void BL_GET_VER_callback(uint8_t* cmd_recive_buffer);
void BL_GET_HELP_callback(uint8_t* cmd_recive_buffer);
void BL_GET_CID_callback(uint8_t* cmd_recive_buffer);
void BL_GET_RDP_STATUS_callback(uint8_t* cmd_recive_buffer);
void BL_GO_TO_ADDRESS_callback(uint8_t* cmd_recive_buffer);
void BL_FLASH_ERASE_callback(uint8_t* cmd_recive_buffer);
void BL_MEM_WRITE_callback(uint8_t* cmd_recive_buffer);
void BL_EN_RW_PROTECT_callback(uint8_t* cmd_recive_buffer);
void BL_MEM_READ_callback(uint8_t* cmd_recive_buffer);
void BL_READ_SECTOR_STATUS_callback(uint8_t* cmd_recive_buffer);
void BL_OTP_READ_callback(uint8_t* cmd_recive_buffer);
void BL_DIS_RW_PROTECT_callback(uint8_t* cmd_recive_buffer);


#endif /* BOOTLOADER_FN_CMD_H_ */
