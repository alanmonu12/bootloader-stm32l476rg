/*
 * bootloader_fn_cmd.c
 *
 *  Created on: 21/12/2018
 *      Author: equipo
 */
#include "DBG.h"
#include "bootloader.h"
#include "bootloader_fn_cmd.h"


/*
 * callback para el comando GET_VER
 */
void BL_GET_VER_callback(uint8_t* cmd_recive_buffer)
{
    uint8_t bl_version;
    uint32_t command_packet_len = (cmd_recive_buffer[0] + LENGHT_BYTE) ;
    uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer+command_packet_len - CRC_BYTES) ) ;

	if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
	{
		Bootloader_send_NACK();
	}
	else
	{
		Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
		bl_version = Bootloader_get_version();
		Bootloader_uart_write_data(&bl_version,1);
	}

}


/*
 * callback para el comando GET_HELP
 */
void BL_GET_HELP_callback(uint8_t* cmd_recive_buffer)
{
		//Comandos soportados por el bootloader
		uint8_t supported_commands[4] = {BL_GET_VER,BL_GET_HELP,BL_GET_CID,BL_GET_RDP_STATUS} ;

        //Total length of the command packet
        uint32_t command_packet_len = cmd_recive_buffer[0] + LENGHT_BYTE ;

        //extract the CRC32 sent by the Host
        uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer + command_packet_len - CRC_BYTES) ) ;

        //Comparacion del CRC
        if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
        {

        	Bootloader_send_NACK();
        }

        else
        {

        	Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
            Bootloader_uart_write_data(supported_commands,sizeof(supported_commands) );

        }

}

/*
 * callback para el comando GET_CID
 */
void BL_GET_CID_callback(uint8_t* cmd_recive_buffer)
{

	uint16_t bl_cid_num = 0;

	//Total length of the command packet
	uint32_t command_packet_len = cmd_recive_buffer[0] + 1;

	//extract the CRC32 sent by the Host
	uint32_t host_crc = *((uint32_t *) (cmd_recive_buffer+command_packet_len - CRC_BYTES) ) ;

     if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
     {

    	 Bootloader_send_NACK();

     }
     else
     {

    	 Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
    	 bl_cid_num = Bootloader_get_DGBMCU();
         Bootloader_uart_write_data((uint8_t*)&bl_cid_num,sizeof(bl_cid_num));

     }
}

/*
 * callback para el comando GET_RDP_STATUS
 */
void BL_GET_RDP_STATUS_callback(uint8_t* cmd_recive_buffer)
{
    uint8_t rdp_level = 0x00;

	//Total length of the command packet
	uint32_t command_packet_len = cmd_recive_buffer[0]+1 ;

	//extract the CRC32 sent by the Host
	uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer+command_packet_len - CRC_BYTES) ) ;

    if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
    {
  	  Bootloader_send_NACK();
    }
    else
    {
        Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
        rdp_level = Bootloader_get_rdp();
        Bootloader_uart_write_data(&rdp_level,sizeof(rdp_level));
    }
}

/*
 * callback para el comando GO_TO_ADDRESS
 */
void BL_GO_TO_ADDRESS_callback(uint8_t* cmd_recive_buffer)
{

	//Total length of the command packet
	uint32_t command_packet_len = cmd_recive_buffer[0] + LENGHT_BYTE;

	//extract the CRC32 sent by the Host
	uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer + command_packet_len - CRC_BYTES) );

    if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
    {
  	  Bootloader_send_NACK();
    }

    else
    {
    	Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
    	Bootloader_JumpToApplication();
    }
}

/*
 * callback para el comando FLASH_ERASE
 */
void BL_FLASH_ERASE_callback(uint8_t* cmd_recive_buffer)
{

	uint8_t erase_status = 0x00;
	uint32_t page_number = 0x00;
	uint32_t number_pages = 0x00;


	//Total length of the command packet
	uint32_t command_packet_len = cmd_recive_buffer[0] + LENGHT_BYTE;

	//extract the CRC32 sent by the Host
	uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer+command_packet_len - CRC_BYTES) ) ;


	page_number = (cmd_recive_buffer[2])  <<  8 | ( cmd_recive_buffer[3]);
	number_pages = (uint32_t) (*(cmd_recive_buffer + 4));

    if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
    {
    	Bootloader_send_NACK();
    }
    else
    {
    	Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));
    	Bootloader_FlashBegin();
    	erase_status = Bootloader_FlashErasePg(page_number,number_pages);
    	HAL_FLASH_Lock();
    	Bootloader_uart_write_data(&erase_status,1);
    }
}

/*
 * callback para el comando MEM_WRITE
 */
void BL_MEM_WRITE_callback(uint8_t* cmd_recive_buffer)
{


	#if defined(AES256)
		uint8_t key[] = { 0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
						  0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4 };
		uint8_t out[] = { 0xf3, 0xee, 0xd1, 0xbd, 0xb5, 0xd2, 0xa0, 0x3c, 0x06, 0x4b, 0x5a, 0x7e, 0x3d, 0xb1, 0x81, 0xf8 };
	#elif defined(AES192)
		uint8_t key[] = { 0x8e, 0x73, 0xb0, 0xf7, 0xda, 0x0e, 0x64, 0x52, 0xc8, 0x10, 0xf3, 0x2b, 0x80, 0x90, 0x79, 0xe5,
						  0x62, 0xf8, 0xea, 0xd2, 0x52, 0x2c, 0x6b, 0x7b };
		uint8_t out[] = { 0xbd, 0x33, 0x4f, 0x1d, 0x6e, 0x45, 0xf2, 0x5f, 0xf7, 0x12, 0xa2, 0x14, 0x57, 0x1f, 0xa5, 0xcc };
	#elif defined(AES128)
		uint8_t key[] = { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };
		//uint8_t out[] = { 0x3a, 0xd7, 0x7b, 0xb4, 0x0d, 0x7a, 0x36, 0x60, 0xa8, 0x9e, 0xca, 0xf3, 0x24, 0x66, 0xef, 0x97 };
	#endif

	struct AES_ctx ctx;

	uint8_t write_status = 0x00;

	uint8_t payload[cmd_recive_buffer[6]];

	uint8_t payload_len = cmd_recive_buffer[6];

	uint32_t mem_address = *((uint32_t *) ( &cmd_recive_buffer[2]) );


    //Total length of the command packet
	uint32_t command_packet_len = cmd_recive_buffer[0]+1 ;

	//extract the CRC32 sent by the Host
	uint32_t host_crc = *((uint32_t * ) (cmd_recive_buffer + command_packet_len - CRC_BYTES) ) ;


	memcpy(payload,cmd_recive_buffer + 7,payload_len);

    AES_init_ctx(&ctx, key);
    AES_ECB_decrypt(&ctx,payload);

	if(Bootloader_get_crc(cmd_recive_buffer,command_packet_len - CRC_BYTES,host_crc) != BL_OK)
    {
    	Bootloader_send_NACK();
    }
    else
    {
    	Bootloader_send_ACK(*((cmd_recive_buffer + LENGHT_BYTE)),*(cmd_recive_buffer));

        //glow the led to indicate bootloader is currently writing to memory
        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);

        //execute mem write

        Bootloader_FlashBegin();
        write_status = Bootloader_ProgrammApp(payload,mem_address);
        HAL_FLASH_Lock();

        //turn off the led to indicate memory write is over
        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

        //inform host about the status
        Bootloader_uart_write_data(&write_status,1);
    }
}

/*
 * callback para el comando EN_RW_PROTECT
 */
void BL_EN_RW_PROTECT_callback(uint8_t* cmd_recive_buffer)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
}

/*
 * callback para el comando MEM_READ
 */
void BL_MEM_READ_callback(uint8_t* cmd_recive_buffer)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
}

/*
 * callback para el comando READ_SECTOR_STATUS
 */
void BL_READ_SECTOR_STATUS_callback(uint8_t* cmd_recive_buffer)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
}

/*
 * callback para el comando OTP_READ
 */
void BL_OTP_READ_callback(uint8_t* cmd_recive_buffer)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
}

/*
 * callback para el comando DIS_RW_PROTECT
 */
void BL_DIS_RW_PROTECT_callback(uint8_t* cmd_recive_buffer)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
}
